from fastapi import FastAPI
from prometheus_fastapi_instrumentator import Instrumentator
import uvicorn
from prometheus_client import Counter

app = FastAPI()

# Создаем кастомную метрику счетчика
REQUEST_COUNTER = Counter('number_of_requests', 'Total number of requests')

# Промежуточное ПО для Prometheus
Instrumentator().instrument(app).expose(app)

@app.get("/hello")
async def hello():
    REQUEST_COUNTER.inc()  # Увеличиваем счетчик на 1 при каждом запросе
    return {"message": "Привет"}

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
